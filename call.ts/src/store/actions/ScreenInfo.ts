import { ScreenData } from 'DataTypes';
import { Actions, ScreenAction } from 'store/actions';

export function screenUpdate(screen: ScreenData): ScreenAction {
  return { type: Actions.SCREEN_UPDATE, screen };
}
