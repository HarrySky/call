import { ScreenData } from 'DataTypes';

export enum Actions {
  CALL_DIALOG_OPEN = 'CD_OPEN',
  CALL_DIALOG_CLOSE = 'CD_CLOSE',
  CALL_DIALOG_SEND = 'CD_SEND',
  CALL_DIALOG_SEND_OK = 'CD_SEND_OK',
  CALL_DIALOG_SEND_FAIL = 'CD_SEND_FAIL',
  CALL_FORM_NAME_CHANGE = 'CF_NAME_CHANGE',
  CALL_FORM_PHONE_CHANGE = 'CF_PHONE_CHANGE',
  CALL_STATUS_OPEN = 'CS_OPEN',
  CALL_STATUS_CLOSE = 'CS_CLOSE',
  SCREEN_UPDATE = 'SCREEN_UPDATE',
}

export interface ScreenAction {
  type: typeof Actions.SCREEN_UPDATE;
  screen: ScreenData;
}

export type CallDialogAction =
  | {
      readonly type: typeof Actions.CALL_DIALOG_OPEN;
    }
  | {
      readonly type: typeof Actions.CALL_DIALOG_CLOSE;
    }
  | {
      readonly type: typeof Actions.CALL_DIALOG_SEND;
    }
  | {
      type: typeof Actions.CALL_DIALOG_SEND_OK;
      status: string;
    }
  | {
      type: typeof Actions.CALL_DIALOG_SEND_FAIL;
      message: string;
    };

export interface CallFormAction {
  type:
    | typeof Actions.CALL_FORM_NAME_CHANGE
    | typeof Actions.CALL_FORM_PHONE_CHANGE;
  value: string;
}

export type CallStatusAction =
  | {
      readonly type: typeof Actions.CALL_STATUS_CLOSE;
    }
  | {
      type: typeof Actions.CALL_STATUS_OPEN;
      message: string;
    };
