import { AppState, INITIAL_STATE } from 'store/State';
import { update } from 'store/utils';
import { Actions, CallStatusAction, CallDialogAction } from 'store/actions';

export function callStatusReducer(
  state = INITIAL_STATE,
  action: CallStatusAction | CallDialogAction
): AppState {
  switch (action.type) {
    case Actions.CALL_STATUS_OPEN:
      return update(state, {
        callStatusOpen: true,
        callStatusMessage: action.message,
      });
    case Actions.CALL_STATUS_CLOSE:
      return update(state, { callStatusOpen: false });
    case Actions.CALL_DIALOG_SEND_OK:
      return update(state, {
        callStatusMessage: action.status,
        callStatusOpen: true,
      });
    default:
      return state;
  }
}
