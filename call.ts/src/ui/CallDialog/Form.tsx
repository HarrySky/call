import Typography from '@material-ui/core/Typography';
import * as React from 'react';
import { connect } from 'react-redux';
import {
  Props,
  mapStateToProps,
  mapDispatchToProps,
} from 'store/ui/CallDialog/Form';
import DialogInput from 'ui/DialogInput';

export class CallForm extends React.PureComponent<Props> {
  render() {
    return (
      <React.Fragment>
        <DialogInput
          label="Name"
          type="text"
          value={this.props.name}
          onChange={this.props.onNameChange}
        />
        <DialogInput
          label="Phone"
          type="tel"
          value={this.props.phone}
          onChange={this.props.onPhoneChange}
        />
        <Typography
          color="error"
          noWrap={true}
          display="block"
          variant="caption"
        >
          {this.props.error}
        </Typography>
      </React.Fragment>
    );
  }
}

// tslint:disable-next-line: no-default-export
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CallForm);
