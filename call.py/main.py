import json
import httpx
from os import getenv
from datetime import datetime
from pytz import timezone
from starlette.applications import Starlette
from starlette.responses import PlainTextResponse
from starlette.requests import Request as StarletteRequest
from starlette import status

SERVER = Starlette()

BOT_TOKEN = getenv('BOT_TOKEN', '12345')
CHAT_ID = getenv('CHAT_ID', '12345')
TIMEZONE = getenv('TIMEZONE', 'UTC')
URL = f'https://api.telegram.org/bot{BOT_TOKEN}/sendMessage'

NOTIFIED = PlainTextResponse('Notified!')
GO_TO_BED = PlainTextResponse('Go To Bed.', status_code=status.HTTP_423_LOCKED)
INADEQUATE_LENGTH = PlainTextResponse('Inadequate Length!', status_code=status.HTTP_400_BAD_REQUEST)
BAD_REQUEST = PlainTextResponse('Bad Request!', status_code=status.HTTP_400_BAD_REQUEST)
TELEGRAM_PROBLEM = PlainTextResponse('Problem on Telegram Side!', status_code=status.HTTP_502_BAD_GATEWAY)

async def send_telegram_notification(message: str) -> bool:
    params = { 'chat_id': CHAT_ID, 'text': message }
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'User-Agent': 'Call/1.1.1'
    }

    # Getting response from Telegram Bot API
    async with httpx.AsyncClient() as client:
        response = await client.get(URL, params=params, headers=headers, timeout=1)

    # Checking if everything is ok
    if not response.json()['ok']:
        return False

    return True

@SERVER.route('/notify', methods=['POST'])
async def notify_endpoint(request: StarletteRequest) -> PlainTextResponse:
    current_hour = datetime.now(tz=timezone(TIMEZONE)).time().hour
    if current_hour < 8:
        return GO_TO_BED

    form = await request.form()
    if 'phone' not in form or 'name' not in form:
        return BAD_REQUEST

    phone = form['phone']
    name = form['name']
    if len(phone) < 4 or len(phone) > 16 or len(name) < 2 or len(name) > 32:
        return INADEQUATE_LENGTH

    message = f'[{name}] is going to call you from {phone}'
    if not await send_telegram_notification(message):
        return TELEGRAM_PROBLEM

    return NOTIFIED
